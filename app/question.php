<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class question extends Model
{
    protected $primaryKey = "qid";

	public function Doctor(){
		return $this->belongsTo('App\User','doctor','uid');
	}
}
