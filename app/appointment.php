<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class appointment extends Model
{
    protected $primaryKey = "aid";

	public function Doctor(){
		return $this->belongsTo('App\User','doctor','uid');
	}

	public function Patient() {
		return $this->belongsTo('App\User','patient','uid');
	}
}
