<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class healthtip extends Model
{
    protected $primayKey = 'hid';

	public $timestamps = false;

	public function Doctor(){
		return $this->belongsTo('App\User','uid','uid');
	}
}
