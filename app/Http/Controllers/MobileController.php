<?php

namespace App\Http\Controllers;

use App\admin;
use App\appointment;
use App\question;
use App\User;
use Exception;
use Illuminate\Http\Request;
use App\healthtip;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class MobileController extends Controller
{
	public function index() {
		return view('welcome');
	}

	public function adminLogin() {
		return view('login');
	}

	public  function login(Request $request){

	    $username = $request->input('u');
	    $password = $request->input('p');

	    try {
		    $user = User::where( 'username', $username )->where( 'password', $password )->first();
		    $user->status = 1;
		    return $user;
	    } catch(\Exception $e){

		    $response = array();
		    $response['status'] = 0;

		    return json_encode($response);

	    }


    }

	public function getHealthTips(){

		$tips = healthtip::all()->sortByDesc("created_at");
		$doctors = array();
		foreach($tips as $item){
			array_push($doctors,$item->Doctor);
		}
		$tips->merge($doctors);

		return json_encode($tips);
	}

	public function sendHealthTips( Request $request ) {
		try {
			$tip    = $request->input( 'content' );
			$title  = $request->input( 'title' );
			$doctor = $request->input( 'uid' );

			$healthtip          = new healthtip();
			$healthtip->title   = $title;
			$healthtip->content = $tip;
			$healthtip->uid     = $doctor;
			$status             = $healthtip->save();

			if($status) echo 1;
		}catch(\Exception $e){

			return 0;

		}

	}

	public function patientPendingAppointments(Request $request) {
		$uid = $request->input('uid');

		$doctors = User::where('role','doctor')->get();
		$appointments = appointment::where('patient',$uid)->get();

		$appointmentItemArray = array();
		foreach($appointments as $item){

			$appointmentItem = array();
			$appointmentItem["date"] = $item->date;
			$appointmentItem["doctor"] = $item->Doctor->name;
			array_push($appointmentItemArray, $appointmentItem);
		}

		sort($appointmentItemArray,SORT_DESC);

		$response = array();
		$response['doctors'] = $doctors;
		$response['appointments'] = $appointmentItemArray;

		echo json_encode($response);
	}

	public function register( Request $request ) {

		try {
			$username = $request->input( 'u' );
			$password = $request->input( 'p' );
			$name     = $request->input( 'name' );
			$email    = $request->input( 'email' );
			$phone    = $request->input( 'phone' );
			$address  = $request->input( 'address' );
			$region   = $request->input( 'region' );
			$country  = $request->input( 'country' );
			$role     = $request->input( 'role' );

			$user = new User();

			$user->username = $username;
			$user->password = $password;
			$user->name     = $name;
			$user->email    = $email;
			$user->phone    = $phone;
			$user->address  = $address;
			$user->region   = $region;
			$user->country  = $country;
			$user->role     = $role;

			$status = $user->save();

			$user->status = $status;
			return $user;

		}
		catch(\Exception $e){

			$response = array();
			$response['status'] = 0;

			return json_encode($response);

		}

	}

	public function askQuestion( Request $request ) {
		$patient = $request->input('uid');
		$question = $request->input('question');

		$questionItem = new question();
		$questionItem->patient = $patient;
		$questionItem->question = $question;
		$questionItem->save();

		echo 1;
	}

	public function getPendingQuestions() {
		$questions = question::where('answer',null)->get();

		echo $questions;
	}

	public function answeredQuestions() {
		$questions = question::all()->sortByDesc("created_at");

		$responseArray = array();
		foreach($questions as $item){
			try {
				if ( $item->doctor != "NULL" ) {
					$response             = array();
					$response['question'] = $item->question;
					$response['answer']   = $item->answer;
					$response['doctor']   = $item->Doctor->name;
					array_push( $responseArray, $response );
				}
			} catch(Exception $e){}

		}
		echo json_encode($responseArray);
	}

	public function answerQuestion(Request $request) {
		$ans = $request->input('answer');
		$qid = $request->input('qid');
		$doctor = $request->input('uid');

		$answer = question::where('qid',$qid)->first();
		$answer->answer = $ans;
		$answer->doctor = $doctor;
		$answer->save();

		echo 1;
	}

	public function patientRequestAppointment( Request $request ) {
		$patient = $request->input('uid');
		$doctor = $request->input('doctor');
		$comment = $request->input('comment');

		$appointment = new appointment();
		$appointment->doctor = $doctor;
		$appointment->patient = $patient;
		$appointment->comment = $comment;
		$appointment->save();

		echo 1;
	}

	public function doctorScheduleAppointment( Request $request ) {
		$aid = $request->input('aid');
		$doctor = $request->input('uid');
		$date = $request->input('date');

		$appointment = appointment::find($aid);

		$appointment->doctor = $doctor;
		$appointment->date = $date;
		$status = $appointment->save();

		if($status) echo 1;
		else echo 0;
	}

	public function doctorPendingAppointments( Request $request ) {
		$doctor = $request->input('uid');
		$appointments = appointment::where('doctor',$doctor)->where('date',null)->get();
		$response = array();
		foreach($appointments as $item){
			$response['appointment'] = $item;
			$response['patient'] = $item->Patient;
		}

		sort($response,SORT_DESC);

		echo json_encode( $appointments );


	}

	public function doctorAppointments( Request $request ) {
		$doctor = $request->input('uid');
		$appointments = appointment::where('doctor',$doctor)->where('date',"<>",null)->get();

		$response = array();
		foreach($appointments as $item){
			$response['appointment'] = $item;
			$response['patient'] = $item->Patient;
		}

		echo json_encode( $appointments );

	}

	public function appointmentHistory(Request $request){
		$doctor = $request->input('doctor');
		$patient = $request->input('patient');

		$patientDetails = User::find($patient);
		$appointments = appointment::where('doctor',$doctor)
			->where('patient',$patient)->get();

		$response['patient']= $patientDetails;
		$response['appointments'] = $appointments;

		echo json_encode($response);
	}

	public function postLogin( Request $request ) {


		$user = admin::where('email', $request->input('email'))->where('password',$request->input('password'))->get();

	echo$user;

		if($user != "[]"){


			return redirect('/manage');
		} else{

			return view('login',['error' => "Wrong credentials"]);
		}

	}


	public function manage() {

		$user = User::where('role','Doctor')->get();

		return view('manage',['users'=> $user]);
	}

	public function delete( $uid ) {
		User::destroy($uid);
		return redirect('/manage');
	}

	public function managePatients() {

		$user = User::where('role','Patient')->get();

		return view('patients',['users' => $user]);
	}


	public function addDoctor() {
		return view('addDoctor');
	}

	public function addPatient() {
		return view('addPatient');
	}


	public function postAddDoctor(Request $request) {
		$user = new User();
		$user->name = $request->input('name');
		$user->username =$request->input('username');
		$user->password = $request->input('password');
		$user->email = $request->input('email');
		$user->region = $request->input('region');
		$user->phone = $request->input('phone');
		$user->address = $request->input('addresss');
		$user->role = "Doctor";
		$user->save();

		return redirect('/manage');

	}

	public function postAddPatient(Request $request) {
		$user = new User();
		$user->name = $request->input('name');
		$user->username =$request->input('username');
		$user->password = $request->input('password');
		$user->email = $request->input('email');
		$user->region = $request->input('region');
		$user->phone = $request->input('phone');
		$user->address = $request->input('addresss');
		$user->role = "Patient";
		$user->save();

		return redirect('/manage');

	}




}
