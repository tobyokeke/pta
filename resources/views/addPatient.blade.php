

<?php session_start(); ?>
        <!DOCTYPE html>
<html lang="en">

<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
    <title>Admin Dashboard | </title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/vendor.css" />
    <link rel="stylesheet" href="css/app-green.css" />
</head>
<body class="">
<nav class="navbar topnav-navbar navbar-fixed-top" role="navigation">
    <div class="navbar-header text-center">
        <button type="button" class="navbar-toggle" id="showMenu" >
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>

        <a class="navbar-brand" href="{{url('/manage')}}"> PTA Admin </a>
    </div>
    <div class="collapse navbar-collapse">



        <ul class="nav navbar-nav pull-right navbar-right">
            <li class="dropdown admin-dropdown">

                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                    <p><span>View</span></p>
                </a>
                <ul class="dropdown-menu" role="menu">
                    <li><a href="{{url('/manage-patients')}}">View Patients</a></li>
                    <li><a href="{{url('/manage')}}">View Doctors</a></li>
                    <li><a href="{{url('/add-doctor')}}">Add Doctor</a></li>
                    <li><a href="{{url('/add-patient')}}">Add Patient</a></li>
                </ul>
            </li>



            <li class="dropdown admin-dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                    <span class="hidden-sm">Administrator</span>
                </a>
                <ul class="dropdown-menu" role="menu">
                    <li><a href="{{url('/admin-login')}}">logout</a></li>
                </ul>
            </li>
        </ul>


    </div>
    <ul class="nav navbar-nav pull-right hidd">
        <li class="dropdown admin-dropdown" dropdown on-toggle="toggled(open)">
            <a href class="dropdown-toggle animated fadeIn" dropdown-toggle><img src="images/flat-avatar.png" class="topnav-img" alt=""></a>
        </li>
    </ul>
</nav>
<section id="body-container" class="animsition dashboard-page">
    <div class="conter-wrapper">



        <div class="col-md-12" style="width:50%; margin-left:200px;">
            <div class="panel panel-default">

                <div class="panel-heading" style="background-color: #43A9A2">
                    <h3 class="panel-title">Add Patient
                        <div class="panel-control pull-right">
                            <a class="panelButton"><i class="fa fa-refresh"></i></a>
                            <a class="panelButton"><i class="fa fa-minus"></i></a>
                            <a class="panelButton"><i class="fa fa-remove"></i></a>
                        </div>
                    </h3>
                </div>
                <div class="panel-body">
                    <form method="post" action="{{url('/add-patient')}}">
                        <div class="form-group">
                            <label for="title">Name</label>
                            <input type="text" pattern="[a-zA-z]+" required name="name" class="form-control underline" id="title" placeholder="Name">
                        </div>
                        <br>
                        <div class="form-group">
                            <label for="title">email</label>
                            <input type="email" name="email" required class="form-control underline" id="title" placeholder="Email">
                        </div>
                        <br>
                        <div class="form-group">
                            <label for="title">Phone</label>
                            <input type="text" name="phone" pattern="[0-9]+" required class="form-control underline" id="title" placeholder="Phone">
                        </div>
                        <br>
                        <div class="form-group">
                            <label for="title">Address</label>
                            <input type="text" name="address" required class="form-control underline" id="title" placeholder="address">
                        </div>
                        <br>
                        <div class="form-group">
                            <label for="title">Region</label>
                            <input type="text" name="region" required class="form-control underline" id="title" placeholder="region">
                        </div>
                        <br>

                        <div class="form-group">
                            <label for="title">Username</label>
                            <input type="text" name="username" required class="form-control underline" id="title" placeholder="Name">
                        </div>
                        <br>

                        <div class="form-group">
                            <label for="title">Password</label>
                            <input type="password" name="password" required class="form-control underline" id="title" placeholder="Name">
                        </div>

                        <br>


                        <button type="submit" class="btn btn-success">Add</button>
                    </form>
                </div>
            </div>


        </div>
    </div>

</section>
<script src="vendor/ckeditor/ckeditor.js" type="text/javascript"></script>
<script src="js/vendor.js"></script>

</body>

</html>
