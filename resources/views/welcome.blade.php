<!DOCTYPE html>
<html lang="en">

<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
    <title>Admin Dashboard | </title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/vendor.css" />
    <link rel="stylesheet" href="css/app-green.css" />
</head>
<body class="">
<nav class="navbar topnav-navbar navbar-fixed-top" role="navigation">
    <div class="navbar-header text-center">
        <button type="button" class="navbar-toggle" id="showMenu" >
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>

        <a class="navbar-brand" href="home.html"> PTA Admin </a>
    </div>
    <div class="collapse navbar-collapse">



        <ul class="nav navbar-nav pull-right navbar-right">
            <li class="dropdown admin-dropdown">

                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                    <p><span>View</span></p>
                </a>
                <ul class="dropdown-menu" role="menu">
                    <li><a href="books.html">View Patients</a></li>
                    <li><a href="videos.html">View Doctors</a></li>
                    <li><a href="videos.html">Add Doctor</a></li>
                </ul>
            </li>



            <li class="dropdown admin-dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                    <img src="images/flat-avatar.png" class="topnav-img" alt=""><span class="hidden-sm">Administrator</span>
                </a>
                <ul class="dropdown-menu" role="menu">
                    <li><a href="profile.html">.profilee</a></li>
                    <li><a href="login.html">.logout</a></li>
                </ul>
            </li>
        </ul>


    </div>
    <ul class="nav navbar-nav pull-right hidd">
        <li class="dropdown admin-dropdown" dropdown on-toggle="toggled(open)">
            <a href class="dropdown-toggle animated fadeIn" dropdown-toggle><img src="images/flat-avatar.png" class="topnav-img" alt=""></a>
            <ul class="dropdown-menu pull-right">
                <li><a href="profile.html">profile</a></li>
                <li><a href="login.html">logout</a></li>
            </ul>
        </li>
    </ul>
</nav>
<section id="body-container" style="margin-left:155px; margin-right:155px;" class="animsition dashboard-page">
    <div class="conter-wrapper">
        <div class="row">
            <div class="col-md-12 "`>
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Doctor
                            <div class="panel-control pull-right">
                                <a class="panelButton"><i class="fa fa-refresh"></i></a>
                                <a class="panelButton"><i class="fa fa-minus"></i></a>
                                <a class="panelButton"><i class="fa fa-remove"></i></a>
                            </div>
                        </h3>
                    </div>
                    <div class="panel-body">
                        <table class="table ">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Address</th>
                                <th>Phone</th>
                                <th>Region</th>
                            </tr>
                            </thead>
                            <tbody>

                            <tr>
                                <td>Joan</td>
                                <td>andygmail.com</td>
                                <td>Mallam</td>
                                <td>023456723</td>
                                <td>Greater Accra</td>
                                <td> <button type="button" class="btn btn-danger btn-rounded">Delete</button></td>
                            </tr>
                            <tr>
                                <td>Frank</td>
                                <td>frank@gmail.com</td>
                                <td>Weija</td>
                                <td>023456723</td>
                                <td>Greater Accra</td>
                                <td> <button type="button" class="btn btn-danger btn-rounded">Delete</button></td>
                            </tr>
                            <tr>
                                <td>John</td>
                                <td>john@gmail.com</td>
                                <td>Kasoa</td>
                                <td>023456723</td>
                                <td>Greater Accra</td>
                                <td> <button type="button" class="btn btn-danger btn-rounded">Delete</button></td>
                            </tr>
                            <tr>
                                <td>Alex</td>
                                <td>andygmail.com</td>
                                <td>Dansoman</td>
                                <td>023456723</td>
                                <td>Greater Accra</td>
                                <td> <button type="button" class="btn btn-danger btn-rounded">Delete</button></td>
                            </tr>
                            <tr>
                                <td>Maame</td>
                                <td>frank@gmail.com</td>
                                <td>Weija</td>
                                <td>023456723</td>
                                <td>Greater Accra</td>
                                <td> <button type="button" class="btn btn-danger btn-rounded">Delete</button></td>
                            </tr>
                            <tr>
                                <td>Joshua</td>
                                <td>john@gmail.com</td>
                                <td>Lapaz</td>
                                <td>023456723</td>
                                <td>Greater Accra</td>
                                <td> <button type="button" class="btn btn-danger btn-rounded">Delete</button></td>
                            </tr>
                            <tr>
                                <td>Samuel</td>
                                <td>andygmail.com</td>
                                <td>Dansoman</td>
                                <td>023456723</td>
                                <td>Greater Accra</td>
                                <td> <button type="button" class="btn btn-danger btn-rounded">Delete</button></td>
                            </tr>
                            <tr>
                                <td>Love</td>
                                <td>frank@gmail.com</td>
                                <td>Weija</td>
                                <td>023456723</td>
                                <td>Greater Accra</td>
                                <td> <button type="button" class="btn btn-danger btn-rounded">Delete</button></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>


</section>
<script src="vendor/ckeditor/ckeditor.js" type="text/javascript"></script>
<script src="js/vendor.js"></script>

</body>

</html>