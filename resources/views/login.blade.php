<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">

<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
    <title>Admin Dashboard | </title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/vendor.css" />
    <link rel="stylesheet" href="css/app-green.css" />
</head>
<body class="">
<nav class="navbar topnav-navbar navbar-fixed-top" role="navigation">
    <div class="navbar-header text-center">
        <button type="button" class="navbar-toggle" id="showMenu" >
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>

        <a class="navbar-brand" href="home.html"> PTA Admin </a>
    </div>
    <div class="collapse navbar-collapse">



        <ul class="nav navbar-nav pull-right navbar-right">


            <li class="dropdown admin-dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                    <img src="images/flat-avatar.png" class="topnav-img" alt=""><span class="hidden-sm">Administrator Login</span>
                </a>

            </li>
        </ul>


    </div>
    <ul class="nav navbar-nav pull-right hidd">
        <li class="dropdown admin-dropdown" dropdown on-toggle="toggled(open)">
            <a href class="dropdown-toggle animated fadeIn" dropdown-toggle></a>

        </li>
    </ul>
</nav>
<section id="body-container" style="margin-left:155px; margin-right:155px;" class="animsition dashboard-page">
    <div class="conter-wrapper">



        <div align="center" class="row col-md-12" style="margin-top: 150px">


            @if(isset($error))
            <div class="alert alert-danger"> {{$error}} </div>
            @endif
            <div class="panel panel-default">
                <div class="panel-heading" align="center">
                    <img src="{{url('img/logo.png')}}" style="width:100px;"> <br>
                    Please login to access to admin portal
                </div>
                <div class="panel-body">
                    <form method="post" action="{{url('/admin-login')}}">

                        <div class="form-group">
                            <label>Email:</label>
                            <input class="form-control" type="text" name="email">
                        </div>

                        <div class="form-group">
                            <label>Password:</label>
                            <input class="form-control" type="password" name="password">
                        </div>

                        <button class="btn btn-primary">Login</button>


                    </form>

                </div>
            </div>
        </div>



    </div>


</section>
<script src="vendor/ckeditor/ckeditor.js" type="text/javascript"></script>
<script src="js/vendor.js"></script>

</body>

</html>