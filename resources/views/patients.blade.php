<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">

<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
    <title>Admin Dashboard | </title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/vendor.css" />
    <link rel="stylesheet" href="css/app-green.css" />
</head>
<body class="">
<nav class="navbar topnav-navbar navbar-fixed-top" role="navigation">
    <div class="navbar-header text-center">
        <button type="button" class="navbar-toggle" id="showMenu" >
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>

        <a class="navbar-brand" href="{{url('/manage')}}"> PTA Admin </a>
    </div>
    <div class="collapse navbar-collapse">



        <ul class="nav navbar-nav pull-right navbar-right">
            <li class="dropdown admin-dropdown">

                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                    <p><span>View</span></p>
                </a>
                <ul class="dropdown-menu" role="menu">
                    <li><a href="{{url('/manage-patients')}}">View Patients</a></li>
                    <li><a href="{{url('/manage')}}">View Doctors</a></li>
                    <li><a href="{{url('/add-doctor')}}">Add Doctor</a></li>
                    <li><a href="{{url('/add-patient')}}">Add Patient</a></li>
                </ul>
            </li>



            <li class="dropdown admin-dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                    <span class="hidden-sm">Administrator</span>
                </a>
                <ul class="dropdown-menu" role="menu">
                    <li><a href="{{url('/admin-login')}}">logout</a></li>
                </ul>
            </li>
        </ul>


    </div>
    <ul class="nav navbar-nav pull-right hidd">
        <li class="dropdown admin-dropdown" dropdown on-toggle="toggled(open)">
            <a href class="dropdown-toggle animated fadeIn" dropdown-toggle><img src="images/flat-avatar.png" class="topnav-img" alt=""></a>
        </li>
    </ul>
</nav>
<section id="body-container" style="margin-left:155px; margin-right:155px;" class="animsition dashboard-page">
    <div class="conter-wrapper">
        <div class="row">
            <div class="col-md-12 "`>
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Patients
                            <div class="panel-control pull-right">
                                <a class="panelButton"><i class="fa fa-refresh"></i></a>
                                <a class="panelButton"><i class="fa fa-minus"></i></a>
                                <a class="panelButton"><i class="fa fa-remove"></i></a>
                            </div>
                        </h3>
                    </div>
                    <div class="panel-body">
                        <table class="table ">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Address</th>
                                <th>Phone</th>
                                <th>Region</th>
                            </tr>
                            </thead>
                            <tbody>


                            @foreach($users as $item)
                                <tr>
                                    <td> {{$item->name}}</td>
                                    <td>{{$item->email}}</td>
                                    <td>Weija</td>
                                    <td>{{$item->phone}}</td>
                                    <td>{{$item->region}}</td>
                                    <td> <a href="{{url('/delete/'. $item->uid)}}" type="button" class="btn btn-danger btn-rounded">Delete</a></td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>


</section>
<script src="vendor/ckeditor/ckeditor.js" type="text/javascript"></script>
<script src="js/vendor.js"></script>

</body>

</html>