<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',function(){
	return redirect('/admin-login');
});

Route::get('/tips','MobileController@getHealthTips');
Route::post('/app-login','MobileController@login');
Route::get('/register','MobileController@register');
Route::post('/send-health-tips','MobileController@sendHealthTips');
Route::post('/get-health-tips','MobileController@getHealthTips');

Route::post('/patient-appointments','MobileController@patientPendingAppointments');


Route::post('/ask-question','MobileController@askQuestion');
Route::post('/get-pending-questions','MobileController@getPendingQuestions');

Route::post('/answer-question','MobileController@answerQuestion');
Route::post('/answered-questions','MobileController@answeredQuestions');


Route::post('/patient-request-appointment','MobileController@patientRequestAppointment');

Route::post('/doctor-schedule-appointment','MobileController@doctorScheduleAppointment');

Route::post('/doctor-pending-appointments','MobileController@doctorPendingAppointments');

Route::post('/doctor-appointments','MobileController@doctorAppointments');

Route::post('/appointment-history','MobileController@appointmentHistory');

Route::get('/admin-login','MobileController@adminLogin');
Route::post('/admin-login','MobileController@postLogin');
Route::get('/manage','MobileController@manage');
Route::get('/manage-patients','MobileController@managePatients');
Route::get('/add-doctor','MobileController@addDoctor');
Route::post('/add-doctor','MobileController@postAddDoctor');


Route::get('/add-patient','MobileController@addPatient');
Route::post('/add-patient','MobileController@postAddPatient');
Route::get('/delete/{uid}','MobileController@delete');
